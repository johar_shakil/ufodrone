
// --------------------------------------------------Testimonials Carousel------------------------------------------------------------------------//
$('.testimonial').slick({
      speed: 1000,
    easing: 'linear',
    pauseOnHover: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    adaptiveHeight: true,
    autoplaySpeed: 2000,
});

$(document).ready(function () {
    $('.set > a').on("click", function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass("active");
            $(this).siblings('.content').slideUp(200);
            $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
        } else {
            $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
            $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
            $(".set > a").removeClass("active");
            $(this).addClass("active");
            $('.content').slideUp(200);
            $(this).siblings('.content').slideDown(200);
        }
    });
});

$('.package1').click(function() { $('.mouse_box_1').show(); $(".mouse_box_2").hide();  $(".mouse_box_3").hide(); });
$('.package2').click(function() { $('.mouse_box_2').show(); $(".mouse_box_1").hide();  $(".mouse_box_3").hide(); });
$('.package3').click(function() { $('.mouse_box_3').show(); $(".mouse_box_2").hide();  $(".mouse_box_1").hide();});

