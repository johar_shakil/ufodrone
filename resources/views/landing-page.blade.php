<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include('partials.header')
<body>
<div class="wrapper">
    <!-- // being topheader -->
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow slideInLeft">
                    <a class="top_logo" href="#" style="overflow: visible">
                        <img src="{{ asset('img/logo.png') }}" class="img-responsive"> </a>
                    <div class="menu_section">

                    </div>
                </div>
                <div class="col-md-6 wow slideInRight">
                    <ul class="list-inline text-md-right">
                        <div class="menu_section">
                            {{ menu('main', 'partials.menus.main') }}
                            @include('partials.menus.main-right')
                        </div>
                        <li class="list-inline-item"> 070 363 368 <br /> <span>  OD 8.00 DO 15.00 </span></li>
                        <!--  <li class="list-inline-item"><i class="fas fa-envelope"></i>  HELLO@UFODRONCEK.COM</li>-->
                    </ul>
                </div>

                {{-- <div class="top-nav container">
                     <div class="top-nav-left">


                     </div>
                     <div class="top-nav-right">

                     </div>
                 </div> <!-- end top-nav -->--}}
            </div>
        </div>
    </div>
    <!-- // end header -->
    <!-- // being header -->
{{--  <header class="header-sticky">
      <nav class="navbar navbar-expand-md navbar-light ">
          <div class="container">
              <a class="navbar-brand wow bounceIn" href="#">
                  <img src="{{ asset('img/logo.png') }}" class="img-responsive"></a>
              <div class="clearfix"></div>
              <!--        <button class="navbar-toggler1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                      </button>-->
              <!-- <div class="collapse1 navbar-collapse1 slideInRight" id="navbarCollapse">
                   <ul class="navbar-nav ml-auto" style="float: right">
                       <li class="nav-item newbutton">

                       </li>
                   </ul>
               </div>-->
          </div>
      </nav>
  </header>--}}
<!--// end header  -->

    <!-- // being main contain -->
    <main role="main">
        <div class="slider">
            <div class="container">
                <div class="newbutton price-button" style="float:right;">
                    69.90 €
                </div>

                <div class="col-md-6 col-sm-6 wow slideInLeft">
                    <div class="slider-left">
                        <h2> UFO DRONCEK  </h2>
                        <h3> Best Flying Gift for Family </h3>
                        <h2>Fun for young & old  </h2>
                        <h5>It provides a great family interaction game that is called "keep it up"!</h5>
                        <div class="orange-button">
                            <a href="#"> Order Now </a>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-sm-6 slideInUp">
                    <div class="slider-right">
                        <img src="{{ asset('img/slider.png') }}" class="img-responsive">
                    </div>
                </div>

            </div>
        </div>
        <div class="product_preview">
            <div class="container">
                <div class="col-md-2 nopadding">
                    <div class="border1"></div>
                </div>
                <div class="col-md-10">
                    <img src="{{ asset('img/ab1.png') }}" class="img-responsive">
                </div>
                <div class="col-md-7 col-sm-7 wow slideInLeft">
                    <div class="box">
                        <h2><span> Product </span> overview </h2>
                        <p> UFO Flying Drone adopt the latest LED infrared sensor hover technology,inductive suspension and collision protection. The built-in gyroscope can control accuracy and sensitivity to make balance. You just need to gently throw the ball in the air, then it can start to fly immediately.</p>
                        <p> A Great Gift to get the kids active and far from the ipad/tablet and TV.</p>

                        <p>
                            Easy to use and control, The Kids are able to quickly grasp how to use the toy and move it in all directions.
                        </p>
                    </div>

                </div>

                <div class="col-md-5 col-sm-5 wow slideInUp">
                    <p>The Latest Infrared Sensor Hover Technology Upgraded in November: The induction sensor is at the bottom and around it, can sensitive to detects nearby objects and is programmed moves away from them.
                    </p>
                    <p>

                        Unexpected Safety and Rechargeable UFO Flying Toys : The propeller was encased by flexible mesh barrier, no touching needed, protect children's eyes and skin from damage. No need to buy a battery, this flying toys is USB safe charging for endless fun!</p>
                </div>
            </div>
        </div>
        <div class="great_gift">
            <div class="container">
                <div class="col-md-8 col-sm-8 wow slideInLeft">
                    <h4>A Great Gift to get the kids active</h4>
                    <h3>UFO DRONCEK Easy to use and control, The Kids are able to quickly grasp how to use the toy and move it in all directions.</h3>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="orange-button wow slideInRight">
                        <a href="#">Buy Now </a>
                    </div>
                </div>
            </div>

        </div>
        <div class="product_feature">
            <div class="container">
                <div class="col-md-6  wow bounceIn">
                    <h3> <span> Product </span>  features </h3>

                    <h5> Multi-sensor, clever as the human brain </h5>
                    <h5>
                        Colorful night lights, LED brilliant lights in the night sky, increase the fun of
                        flying at night
                    </h5>
                    <ul> <li> High toughness protection, no fear of collision,               effectively reduce impact injury, prevent finger injury</li>
                        <li>Trendy products, new patterns, interactive entertainment, easy to use. </li>
                        <li>Intelligent automatic obstacle avoidance, visible intelligent objects.
                        </li>
                        <li>Gesture control flight, no remote control, easy to use.
                        </li>
                        <li>360 stunt rotation, more interesting flight experience.
                        </li>
                        <li> Inverted stop flight, humanized safety protection.
                        </li>
                        <li> USB smart charging, not afraid of charger loss.
                        </li>

                        <li> Throw fly, so simple, blue light can take off.
                        </li>
                        <li> Feel sensitive, full of fun </li>

                    </ul>

                    <p> It is an interactive flying toy of personality! Toss it up and will fly like no other drone you've seen before! </p>
                </div>
                <div class="col-md-6  bg_feature">
                </div>
            </div>
        </div>
        <div class="product_spcification">
            <div class="container">
                <h2><span> Product </span> Specifications </h2>
                <div class="col-md-12 wow bounceIn">
                    <img src=" {{ asset('img/red_fan.png') }}" class="img-responsive">
                </div>
                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <div class="col-md-6 col-sm-6  nopadding wow slideInLeft">
                        <ul>
                            <li> Material: ABS plastic + Electronic original </li>
                            <li> Flying time: About 10 minutes </li>
                            <li>Charging time: About 30 minutes.</li>
                            <li>Color: Red/ Blue/ Gold.</li>
                        </ul>
                    </div>
                    <div class="col-md-1 col-sm-1"></div>
                    <div class="col-md-5 col-sm-5 nopadding wow slideInRight">
                        <ul>
                            <li>  Size: 11 x 11 x 6cm. </li>
                            <li> Type: Sensing toy. </li>
                            <li> Weight: 300g. </li>
                            <li>  Detailed information: </li>
                        </ul>
                    </div>
                </div>
                <!--  <div class="col-md-3"></div>-->
            </div>
        </div>
        <div class="great_gift">
            <div class="container">
                <div class="col-md-8 col-sm-8 wow bounceInLeft">
                    <h4>A Great Gift to get the kids active</h4>
                    <h3>UFO DRONCEK Easy to use and control, The Kids are able to quickly grasp how to use the toy and move it in all directions.</h3>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="orange-button bounceInRight">
                        <a href="#">Buy Now </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_preview product_warrenty">
            <div class="container">

                <div class="col-md-10">
                    <img src="{{ asset('img/fan2.png') }}" class="img-responsive">
                </div>
                <div class="col-md-2 nopadding">
                    <div class="border2"></div>
                </div>
                <div class="col-md-5">

                </div>
                <div class="col-md-7  wow slideInUp">
                    <div class="box">
                        <h2> warranty </h2>
                        <p>1-year warranty, buy with confidence.</p>
                        <p>  If you're not satisfied with the item while it is in good condition, please return it to us within 30 days. We will either send you a new one or give your money back.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_discount">
            <div class="container">
                <h1> <span>BUY MORE</span>  GET DISCOUNT (3 QUANTITY OPTIONS) </h1>
                <div class="col-md-4 col-sm-4  wow slideInLeft">
                    <div class="product_box">
                        <div class="product_img">
                            <img src="{{ asset('img/box1.png') }}" class="img-responsive">
                            <div class="discount_cart">
                                <img src="{{ asset('img/dis1.png') }}">
                            </div>
                        </div>
                        <div class="product_content">
                            <h4> UFO DEONCEK </h4>
                            <h3>  1x 39,90€ </h3>
                            <div class="purchased_now">
                                <a href="#"> purchase now </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 wow bounceIn">
                    <div class="product_box">
                        <div class="product_img">
                            <img src="{{ asset('img/box4.png') }}" class="img-responsive">
                            <div class="discount_cart">
                                <img src="{{ asset('img/ticker2.png') }}">
                            </div>
                        </div>
                        <div class="product_content">
                            <h4> UFO DEONCEK </h4>
                            <h3>  1x 39,90€ </h3>
                            <div class="purchased_now">
                                <a href="#"> purchase now </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 wow slideInRight">
                    <div class="product_box">
                        <div class="product_img">
                            <img src="{{ asset('img/box2.png') }}" class="img-responsive">
                            <div class="discount_cart">
                                <img src="{{ asset('img/ticker3.png') }}">
                            </div>
                        </div>
                        <div class="product_content">
                            <h4> UFO DEONCEK </h4>
                            <h3>  1x 39,90€ </h3>
                            <div class="purchased_now">
                                <a href="#"> purchase now </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="great_gift">
            <div class="container">
                <div class="col-md-8 col-sm-8 wow slideInLeft">
                    <h4>A Great Gift to get the kids active</h4>
                    <h3>UFO DRONCEK Easy to use and control, The Kids are able to quickly grasp how to use the toy and move it in all directions.</h3>
                </div>
                <div class="col-md-4 col-sm-4 wow slideInRight">
                    <div class="orange-button">
                        <a href="#">Buy Now </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_preview product_delivery">
            <div class="container">
                <div class="col-md-2">
                    <div class="border1"></div>
                </div>
                <div class="col-md-10 ">
                    <img src="{{ asset('img/deliver.png') }}" class="img-responsive">
                </div>
                <div class="col-md-7 wow slideInUp">
                    <div class="box">
                        <h2><span> Product </span>  delivery </h2>
                        <p>0-40€  Delivery 3,99€ </p>
                        <p>  <b> Above 40€ free shipping ! </b></p>
                    </div>
                </div>
                <div class="col-md-5">
                </div>
            </div>
        </div>
        <div class="product_video">
            <div class="container">
                <h2> <span>how it</span> works </h2>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="laptop-wrapper wow fadeIn">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/XvMdL1PncEQ" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_color_option">
            <div class="container">
                <h2> <span> Product </span>  color option </h2>
                <div class="col-md-4 col-sm-4 wow slideInLeft">
                    <div class="product_box">
                        <div class="product_img">
                            <img src="{{ asset('img/p1.png') }}" class="img-responsive">
                            <div class="discount_cart2">
                                <img src="{{ asset('img/ticker.png') }}">
                            </div>
                        </div>

                        <div class="product_content">
                            <h4> UFO DEONCEK  Red </h4>
                            <h3> 69,90€ </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 wow slideInUp">
                    <div class="product_box">
                        <div class="product_img">
                            <img src="{{ asset('img/p2.png') }}" class="img-responsive">
                            <div class="discount_cart2">
                                <img src="{{ asset('img/ticker.png') }}">
                            </div>
                        </div>
                        <div class="product_content">
                            <h4> UFO DEONCEK Gold </h4>
                            <h3> 69,90€ </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 wow slideInRight">
                    <div class="product_box">
                        <div class="product_img">
                            <img src="{{ asset('img/p4.png ') }}" class="img-responsive">
                        </div>
                        <div class="product_content">
                            <h4> UFO DEONCEK Blue </h4>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonail wow bounceIn">
            <div class="container">
                <div class="testimonail_box">

                    <div class="testimonial">
                        <div class="loop">
                            <!--   <div class="content">
                                   <div class="rating">
                                       <img src="assets/img/rating.png" class="img-responsive">
                                   </div>
                                   <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex </p>
                               </div>-->

                            <img src="{{ asset('img/fbwidget.png') }}" class="img-responsive">

                            <!-- <div class="title">
                                 John Wick
                             </div>-->
                        </div>
                        <div class="loop">

                            <img src=" {{asset('img/fbwidget.png')}}" class="img-responsive">

                            <!--     <div class="content">
                                     <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex </p>
                                 </div>-->

                            <!--  <div class="title">
                                  John Wick
                              </div>-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="order_confirmation wow slideInUp">
            <div class="container">
                <div class="col-md-3 col-sm-3 nopadding">
                    <div class="hover_block_button_2 mouse_box_1 mouse_box_block">
                        <div class="seal_section">
                            <del> 19.90 </del>
                            <h2> 19.90 </h2>
                        </div>
                        <div class="save_money">
                            <h1> save-0% </h1>
                            <img src=" {{ asset('img/payment.png') }}" style="width: auto">
                            <img src="{{ asset('img/free_delivery.png') }}" class="img-responsive">
                        </div>
                    </div>
                    <div class="hover_block_button_2 mouse_box_2">
                        <div class="seal_section">
                            <del>19.92 </del>
                            <h2> 15.92 </h2>
                        </div>
                        <div class="save_money">
                            <h1> save-20% </h1>
                            <img src=" {{ asset('img/payment.png') }}" style="width: auto">
                            <img src="{{ asset('img/free_delivery.png') }}" class="img-responsive">
                        </div>
                    </div>
                    <div class="hover_block_button_2 mouse_box_3">
                        <div class="seal_section">
                            <del>  19.90  </del>
                            <h2> 13.93 </h2>
                        </div>
                        <div class="save_money">
                            <h1> save-30% </h1>
                            <img src="{{ asset('img/payment.png') }}" style="width: auto">
                            <img src="{{ asset('img/free_delivery.png') }}" class="img-responsive">
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-sm-6 nopadding">
                    <div class="order_confirmation_inner">
                        <h2> <span> order  </span>   information </h2>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Name & surname">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Address">
                            </div>
                            <div class="col-md-6 col-sm-12 padleft">
                                <div class="form-group">
                                    <input type="text" class="form-control"  placeholder="House #">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 padright">
                                <div class="form-group">
                                    <input type="text" class="form-control"  placeholder="Postal Code">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"  placeholder="City">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control"  placeholder="email address">
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control"  placeholder="phone #">
                            </div>
                            <div class="col-md-6 col-sm-12 padleft">
                                <div class="form-group">
                                    <select>
                                        <option>Product Color</option>
                                        <option> Red </option>
                                        <option> Blue </option>
                                        <option> Gold </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 nopadding">
                                <div class="form-group">
                                    <input type="number" class="form-control"  placeholder="Product Quanity">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 col-sm-12 padleft">
                                <input type="button" name="package1" class="package1" value="1 za  19.90€/Kos  -0 %">
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <input type="button" name="package2" class="package2" value="2 za  15.92€/Kos  -20 %">
                            </div>
                            <div class="col-md-4 col-sm-12 padright">
                                <input type="button" name="package3" class="package3" value="3 za  13.93€/Kos  -30 %">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-check nopadding">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">I agree with the Terms and Conditions.</label>
                            </div>
                            <br />
                            <div class="form-check nopadding">
                                <input type="checkbox" class="form-check-input" id="exampleCheck2">
                                <label class="form-check-label" for="exampleCheck2">I agree with the Terms and Privacy.</label>
                            </div>

                            <h3> Select a payment method. </h3>

                            <div class="form-group">
                                <select>
                                    <option> Cash on delivery </option>
                                    <option> Credit card </option>
                                    <option> Paypal </option>
                                </select>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">confirm your oder</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 nopadding mouse_box_1 ">
                    <div class="hover_block_button_1">
                        <img src="{{ asset('img/hover_block.png') }}">
                    </div>
                </div>
                <div class="col-md-3 nopadding mouse_box_2">
                    <div class="hover_block_button_1">
                        <img src="{{ asset('img/hover_block.png') }}">
                    </div>
                </div>
                <div class="col-md-3 nopadding mouse_box_3">
                    <div class="hover_block_button_1">
                        <img src="{{ asset('img/hover_block.png') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="frequntly_asked_question">
            <div class="container">
                <h2> <span> Frequently asked  </span> questions </h2>
                <section class="section">
                    <accordion>
                        <div class="accordion-container">
                            <div class="col-md-6 col-sm-6 wow slideInLeft">
                                <div class="set">
                                    <a href="javascript:void(0)">
                                        Vestibulum

                                    </a>
                                    <div class="content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                    </div>
                                </div>
                                <div class="set">
                                    <a href="javascript:void(0)">
                                        Phasellus

                                    </a>
                                    <div class="content">
                                        <p> Aliquam cursus vitae nulla non rhoncus. Nunc condimentum erat nec dictum tempus. Suspendisse aliquam erat hendrerit vehicula vestibulum.</p>
                                    </div>
                                </div>
                                <div class="set">
                                    <a href="javascript:void(0)">
                                        Praesent asas

                                    </a>
                                    <div class="content">
                                        <p>Pellentesque aliquam ligula libero, vitae imperdiet diam porta vitae. sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 wow slideInRight">
                                <div class="set">
                                    <a href="javascript:void(0)">
                                        Curabitur

                                    </a>
                                    <div class="content">
                                        <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
                                    </div>
                                </div>
                                <div class="set">
                                    <a href="javascript:void(0)">
                                        Phasellus

                                    </a>
                                    <div class="content">
                                        <p> Aliquam cursus vitae nulla non rhoncus. Nunc condimentum erat nec dictum tempus. Suspendisse aliquam erat hendrerit vehicula vestibulum.</p>
                                    </div>
                                </div>
                                <div class="set">
                                    <a href="javascript:void(0)">
                                        Praesent asas

                                    </a>
                                    <div class="content">
                                        <p>Pellentesque aliquam ligula libero, vitae imperdiet diam porta vitae. sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </accordion>
                </section>
            </div>
        </div>
    </main>
    <!-- // end main contain -->
    <footer class="footer">
        <div class="container">
            <div class="chat_icon">
                <img src="{{ asset('img/chat_icon.png') }}">
            </div>
            <div class="col-md-3 col-sm-3 wow slideInLeft">
                <div class="footer_title">
                    <h4>
                        Our Company
                    </h4>
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor .
                </p>
                <div class="small_title">
                    Newsletter Signup
                </div>
                <form>
                    <div class="form-group">
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email">
                        <button type="submit" class="btn btn-primary"> <img src="{{ asset('img/submit.png') }}"></button>
                    </div>
                </form>
            </div>
            <div class="col-md-3 col-sm-3 footer_sec_second wow bounceInLeft">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut  </p>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer_title">
                    <h4>
                        List
                    </h4>
                </div>
                <ul> <li> Terms of Privacy</li>
                    <li> Terms of Condifitions </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 footer_sec_fourth wow bounceInUp">
                <div class="footer_title">
                    <h4>
                        Our Locations
                    </h4>
                    <p>
                        967 Poor House Lane
                        Mountain View, CA 94043
                    </p>
                    <div class="phone">
                        <a href="tel:+123-456-789">+123-456-789</a>
                    </div>
                    <div class="social_link">
                        <ul>
                            <li><a href="https://facebook.com"> <i class="fa fa-facebook" aria-hidden="true"></i> </a></li>
                            <li> <a href="https://instagram.com"> <i class="fa fa-instagram" aria-hidden="true"></i> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<div id="app">
    <header class="with-background">
        <div class="top-nav container">
            <div class="top-nav-left">
                <div class="logo"> <img src="{{ asset('img/logo.png') }}">   </div>
                {{ menu('main', 'partials.menus.main') }}
            </div>
            <div class="top-nav-right">
                @include('partials.menus.main-right')
            </div>
        </div> <!-- end top-nav -->
        <div class="hero container">
            <div class="hero-copy">
                <h1>UFODRONCEK</h1>
                <p>Includes multiple products, categories, a shopping cart and a checkout system with Stripe integration.</p>
                <div class="hero-buttons">

                </div>
            </div> <!-- end hero-copy -->

            <div class="hero-image">
                <img src="img/macbook-pro-laravel.png" alt="hero image">
            </div> <!-- end hero-image -->
        </div> <!-- end hero -->
    </header>

    <div class="featured-section">

        <div class="container">
            <h1 class="text-center">Laravel Ecommerce</h1>

            <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore vitae nisi, consequuntur illum dolores cumque pariatur quis provident deleniti nesciunt officia est reprehenderit sunt aliquid possimus temporibus enim eum hic lorem.</p>

            <div class="text-center button-container">
                <a href="#" class="button">Featured</a>
                <a href="#" class="button">On Sale</a>
            </div>

            {{-- <div class="tabs">
                <div class="tab">
                    Featured
                </div>
                <div class="tab">
                    On Sale
                </div>
            </div> --}}

            <div class="products text-center">
                @foreach ($products as $product)
                    <div class="product">
                        <a href="{{ route('shop.show', $product->slug) }}"><img src="{{ productImage($product->image) }}" alt="product"></a>
                        <a href="{{ route('shop.show', $product->slug) }}"><div class="product-name">{{ $product->name }}</div></a>
                        <div class="product-price">{{ $product->presentPrice() }}</div>
                    </div>
                @endforeach

            </div> <!-- end products -->

            <div class="text-center button-container">
                <a href="{{ route('shop.index') }}" class="button">View more products</a>
            </div>

        </div> <!-- end container -->

    </div> <!-- end featured-section -->

    {{-- <blog-posts></blog-posts>--}}

    @include('partials.footer')

</div> <!-- end #app -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>
<script src="{{ asset('js/main2.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>
